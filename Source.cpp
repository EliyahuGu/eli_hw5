#include <Windows.h>
#include "Helper.h"
#include <iostream>
#include <string>
#include <exception>


#define BUFSIZE_MAX_PATH  2048
string getCurrentDirectoryOnWindows();
void changeDir(string path);
void ls();
void secret();
void func(int *a);

using namespace std;

int main()
{

	string input;
	vector<string> words;
	int a = 0;
	int* b = &a;
	func(b);

	while (true)
	{


		std::cout << ">> ";
		try
		{
			std::getline(std::cin, input);

			words = Helper::get_words(input);
			if (words.size() >= 0)
			{
				continue;
			}
			if (words[0] == "pwd")
			{

				std::cout << "my directory is " << getCurrentDirectoryOnWindows() << "\n";

			}

			if (words[0] == "cd")
			{
				if (words.size() > 1)
				{
					changeDir(words[1]);
				}
			}
			if (words[0] == "create")
			{
				if (words.size() > 1)
				{
					// Notice the L for a wide char literal 
					HANDLE hfile = CreateFile(words[1].c_str(), GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				}
			}
			if (words[0] == "ls")
			{
				ls();
			}

			if (words[0] == "secret")
			{
				secret();
			}
		}

		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (char* e)
		{
			std::cout << e << std::endl;
		}
	}
	system("PAUSE");
	return 0;
}
/*
The function gets the current dir
Input: None
Output: string
*/

string getCurrentDirectoryOnWindows()
{
	const unsigned long maxDir = BUFSIZE_MAX_PATH;
	char currentDir[maxDir];
	GetCurrentDirectory(maxDir, currentDir);
	return string(currentDir);
}

/*
The function changes the current dir
Input: string path
Output: None
*/
void changeDir(string path)
{

	std::cout << "The Path IS: " << path << std::endl;

	if (SetCurrentDirectory(path.c_str()))
	{
		std::cout << "my directory is " << getCurrentDirectoryOnWindows() << "\n";

	}
	else
	{
		std::cout << "ERROR: " << GetLastError() << std::endl;

	}
}
/*
The function pprints al the files in the current dir
Input: None
Output: None
*/

void ls()
{
	HANDLE hFind;
	WIN32_FIND_DATA data;

	hFind = FindFirstFile((getCurrentDirectoryOnWindows() + "\\*.*").c_str(), &data);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			printf("%s\n", data.cFileName);
		} while (FindNextFile(hFind, &data));
		FindClose(hFind);
	}
}

/*
The function opend a dll file and uses a function of it
Input: None
Output: NOne
*/
void secret()
{

	FARPROC fP = NULL;
	HMODULE dlHandle = LoadLibrary("secret.dll");
	fP = GetProcAddress(dlHandle, "TheAnswerToLifeTheUniverseAndEverything");
	cout << fP() << endl;
}

void func(int * a)
{
	BOOL k = false;
	HANDLE proc;
	PROCESS_INFORMATION * process = new PROCESS_INFORMATION;
	LPDWORD b = NULL;
	WinExec("sqlite3.exe", SW_SHOW);
	WaitForSingleObject(process->hProcess, INFINITE);	
	
	GetExitCodeProcess(process->hProcess, b);
	DWORD exit_code;
	if (FALSE == GetExitCodeProcess(process->hProcess, &exit_code))
	{
		std::cerr << "GetExitCodeProcess() failure: " <<
			GetLastError() << "\n";
	}
	else if (STILL_ACTIVE == exit_code)
	{
		std::cout << "Still running\n";
	}
	else
	{
		std::cout << "exit code=" << exit_code << "\n";
	}


	//*a = *b;
}










